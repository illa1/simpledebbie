define([
    "jquery",
    "underscore", 
    "bootstrap",

    "save_changes_view",
    "editor_manager",
    
    "text!templates/load.html",
    "text!templates/current_name.html",
], function ($, _, bootstrap, saveChangesView, editorManager, loadTemplate, currentNameTemplate) {

    var	choice;

    $("#show-load-modal").click(_loadFile);
    
    function _loadFile() {
        saveChangesView.check(_renderLoadModal);
    }

    function _renderLoadModal() {
        var files = JSON.parse(localStorage.debbie_programs);
        choice = undefined;
        $("#load-table").html(_.template(loadTemplate, {programs: files}));
        $('#load-modal').modal('show');
        $('#load-table .choice').click(_highlight);
        $("#load-ok").click(_load);
        $(".remove").click(_remove);
    }

    function _remove() {
        var	toRemoveName = $(this).parent().parent().attr('id'),
            files = files = JSON.parse(localStorage.debbie_programs);

        delete files[toRemoveName];
        localStorage.debbie_programs = JSON.stringify(files);
        if (localStorage.debbie_current == toRemoveName) {
            localStorage.debbie_current = '...';
            editorManager.setText(files['...'].text);
            $('#current-program').html('...');
        }
        _renderLoadModal();
    }

    function _highlight() {
        $('#load-table .selected-program').removeClass('selected-program');
        $(this).addClass('selected-program');
        choice = $(this).attr('id');
    }

    function _load() {
        if (choice != undefined) {
            localStorage.debbie_current = choice;
            loadCurrent();
            $('#load-modal').modal('hide');
        }
    }

    function loadCurrent() {
        var files = JSON.parse(localStorage.debbie_programs),
            current = localStorage.debbie_current;
            
        editorManager.setText(files[current].text);
        editorManager.setOnTextChangeListener(_onTextChange);
        $("#current-program").html(_.template(currentNameTemplate, {changed: false, fileName: current}));
    }

    function _onTextChange() {
        var current = localStorage.debbie_current;
        if (current != '...') {
            $("#current-program").html(_.template(currentNameTemplate, {changed: true, fileName: current}));                
        }
    }

    return {
        loadCurrent: loadCurrent,
    }
});
