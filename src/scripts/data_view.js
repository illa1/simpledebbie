define([
    "jquery",
    "underscore",
    "bootstrap",
    
    "edit_handler",

    "text!templates/data_top.html",
    "text!templates/data.html",
], function($, _, bootstrap, EditHandler, dataTopTemplate, dataTemplate) {

    var dataView = function(title, divTopName, divName, choices, getValues, setValue, maxOnPage) {
        var choices = _split(choices),
            page = 0,
            addressRepresentation = 10,
            dataRepresentation = 10,
            nextRepresentation = {10: 16, 16: 2, 2: 10},
            editHandler = new EditHandler(divName, _afterEdit);
            
        $(divTopName).html(_.template(dataTopTemplate, {name: title, choices: choices}));
        $(divTopName + ' .dropdown-toggle').dropdown();

        $(divTopName + ' .data-choice').click(function() {
            page = $(this).parent().children().index(this);
            render();
        });
        $(divTopName + " .data-representation").click(_changeDataRepresentation);
        $(divTopName + " .address-representation").click(_changeAddressRepresentation);

        function _afterEdit(el, index) {
            setValue(choices[page].start + index, parseInt(el.val(), dataRepresentation));
            return _toCurrentDataRepresentation(getValues()[choices[page].start + index]);
        }

        function _split(oldChoices) {
            var result = [],
                start,
                end;
            for (var i = 0; i < oldChoices.length; i+= 1) {
                start = oldChoices[i].start;
                end = oldChoices[i].end;
                while (end - start > maxOnPage) {
                    result.push({name: oldChoices[i].name, start: start, end: start+maxOnPage, delta: oldChoices[i].delta});
                    start += maxOnPage;
                }
                result.push({name: oldChoices[i].name, start: start, end: end, delta: oldChoices[i].delta});
            }
            return result;
        }

        function _changeDataRepresentation() {
            var oldRepresentation = dataRepresentation;
            dataRepresentation = nextRepresentation[dataRepresentation];
            $(divName + ' .val').each(function() {
                if ($(this).prop("tagName") == 'INPUT') {
                    $(this).val(_toCurrentDataRepresentation(parseInt($(this).val(), oldRepresentation)));
                } else {
                    $(this).html(_toCurrentDataRepresentation(parseInt($(this).html(), oldRepresentation)));
                }
            });
        }

        function _changeAddressRepresentation() {
            var oldRepresentation = addressRepresentation;
            addressRepresentation = nextRepresentation[addressRepresentation];;
            $(divName + ' .address').each(function() {
                $(this).html(_toCurrentAddressRepresentation(parseInt($(this).html(), oldRepresentation)));
            });
        }

        function _toCurrentDataRepresentation(value) {
            if (dataRepresentation == 10) return value;
            value = value.toString(dataRepresentation).toUpperCase();
            while (dataRepresentation == 2 && value.length < 8) {
                value = '0' + value;
            }
            if (dataRepresentation == 16 && value.length < 2) {
                value = '0' + value;
            }
            return value;
        }

        function _toCurrentAddressRepresentation(value) {
            if (addressRepresentation == 10) return value;
            value = value.toString(addressRepresentation).toUpperCase();
            while (addressRepresentation == 2 && value.length < 8) {
                value = '0' + value;
            }
            if (addressRepresentation == 16 && value.length < 2) {
                value = '0' + value;
            }
            return value;
        }

        function render() {
            editHandler.endEdit();
            $(divName).html(_.template(dataTemplate, {values: getValues(), choice: choices[page], _toCurrentDataRepresentation: _toCurrentDataRepresentation, _toCurrentAddressRepresentation: _toCurrentAddressRepresentation}));
            $( divTopName + ' .data-choice').each(function() {
                if ( $(this).index() == page) {
                    $(this).addClass('active');
                } else {
                    $(this).removeClass('active');
                }
            });
            $(divName + ' .val').dblclick(function() {
                var index = $(divName + ' .val').index(this);
                if (index >= 0) {
                    editHandler.startEdit(index, dataRepresentation);
                }
            });
        }

        function change(changes) {
            var start = choices[page].start + choices[page].delta,
                end = choices[page].end + choices[page].delta,
                lastChangesRemoved = false;

            if (changes.length <= 0) return;

            changes.forEach( function(c) {
                if (start <= c.index && c.index < end) {
                    if (lastChangesRemoved == false) {
                        lastChangesRemoved = true;
                        $( divName + " .val" ).removeClass('last-changed');
                    }
                    $( divName + " .val:eq( "+(c.index-start)+" )" ).addClass('last-changed');
                    $( divName + " .val:eq( "+(c.index-start)+" )" ).html(_toCurrentDataRepresentation(c.value));
                }
            });
        }
        
        function highlight(index) {
            var start = choices[page].start + choices[page].delta,
                end = choices[page].end + choices[page].delta;
            if (start <= index && index < end) {
                $( divName + " .val").removeClass('highlight-background');
                $( divName + " .val:eq( "+(index-start)+" )" ).addClass('highlight-background');
            }
        }

        return {
            render: render,
            change: change,
            highlight: highlight,
        }
    };

    return dataView;
});
