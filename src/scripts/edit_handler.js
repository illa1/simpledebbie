define([
    "jquery",
], function ($) {

    var editHandler = function(divName, callback) {
        var input = -1,
            timeout = 60000;

        function startEdit(index, dataRepresentation) {
            var el,
                dataRepresentation = dataRepresentation == undefined ? 10 : dataRepresentation,
                size = 2;
                
            if (dataRepresentation == 2) {
                size = 7;
            }
            if (input >= 0) {
                clearTimeout(timer);
                endEdit();
            }
            input = index;
            el = $( divName + " .val:eq( "+input+" )" );
            el.removeClass('val');
            el.html("<input autofocus='true' type='text' class='val' size='"+size+"' value='" +el.html()+ "'/>");
            el.children().first().focus();
            timer = setTimeout(endEdit, timeout);
            $(divName + ' input').keyup(function(event) {
                if (event.keyCode == 13) {
                    clearTimeout(timer);
                    endEdit();
                } else {
                    clearTimeout(timer);
                    timer = setTimeout(endEdit, timeout);
                }
            });
        }

        function endEdit() {
            var el, value;
            if (input >= 0) {
                el = $( divName + " .val:eq( "+input+" )" );
                value = callback(el, input);
                el.parent().addClass('val');
                el.parent().html(value);
            }
            input = -1;
        }

        return {
            startEdit: startEdit,
            endEdit: endEdit,
        }
    };

    return editHandler;
});
