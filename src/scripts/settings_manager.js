define([
    "jquery",
    "underscore",
    
    "local_storage_tester",

    "text!templates/settings.html",
], function($, _, storageTester, settingsTemplate) {

    var subscribers = [],
        storage = storageTester.isAvaible(),
        defaultSettings = {
            "Processor": {values: [], checked: 0},
            "Theme":     {values: ["light", "dark"], checked: 0},
//          "Layout":    {values: ["editor at left", "editor in center", "editor at right"], checked: 0},
        };
                    
    function _render() {
        $('#settings-body').html(_.template(settingsTemplate, {settings: settings}));                
        $('#settings-modal').modal('show');
    }

    function _save() {
        for (s in settings) {
            if (settings[s].checked != $('input[name='+s+']:checked').val()) {
                settings[s].checked = $('input[name='+s+']:checked').val();
                _notify(s, settings[s].values[settings[s].checked]);
            }
        }
		if (storage == true) {
			localStorage.debbie_settings = JSON.stringify(settings);
		}
        $('#settings-modal').modal('hide');
    }

    function _notify(name, value) {
        subscribers.forEach(function(s) {
            s(name, value);
        });
    }

    function register(subscriber) {
        if (subscribers.indexOf(subscriber) < 0) {
            subscribers.push(subscriber);
        }
    }

    function init(processorList) {
        defaultSettings["Processor"].values = processorList;
        if (storage == true) {
            if (localStorage.getItem("debbie_settings") != null) {
                settings = JSON.parse(localStorage.getItem("debbie_settings"));
                for (s in defaultSettings) {
                    if (defaultSettings[s].values.length > settings[s].checked) {
                        defaultSettings[s].checked = settings[s].checked;
                    }
                }
            }
        }
        settings = defaultSettings;
        if (storage == true) {
            localStorage.setItem("debbie_settings", JSON.stringify(settings));
        }
        for (s in settings) {
            _notify(s, settings[s].values[settings[s].checked]);
        }
        $('#settings-show').click(_render);
        $('#save-settings').click(_save);
    }
    
    return {
        init: init,
        register: register,
    };
        
});
