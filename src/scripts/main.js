require.config({
    paths: {
        "jquery": "../lib//jquery",
        "jquery-ui": "../lib/jquery-ui",
        "ace": "../lib/ace/ace",
        "underscore": "../lib/underscore",
        "bootstrap": "../lib/bootstrap",
        "text": "../lib/text",
        "templates": '../templates',
    },
    shim: {
        "jquery-ui": {
            exports: "ui",
            deps: ['jquery']
        },
        "ace": {
            exports: "ace",
            deps: []
        },
        "bootstrap": {
            exports: "bootstrap",
            deps: ['jquery']
        },
    },
});

require(["debbie", "processors/at90/at90"], function(debbie, at90) {
    debbie.addProcessor(at90);
    debbie.start();
});
