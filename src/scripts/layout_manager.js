define([
    "jquery",
    "jquery-ui",

    "editor_manager",
], function($, ui, editorManager) {

    function init() {
        _resizeLayout();
        _setLayoutListeners();
    }
    
    function _resizeLayout() {
        var width = $(window).width(),
            height = $(window).height();
        $("#editor-container").width(width/2 - 12);
        $(".status-container").width(width/2 - 12);
        $(".info-container").width(width/4 - 12);
        $("#editor-container").height(height - $("#code").outerHeight(true) + $("#editor-container").height() - $("#status").outerHeight(true) - $("#panel").outerHeight(true));
        $("#registers-all .info-container").height(height - $("#registers-all").outerHeight(true) + $("#registers-all .info-container").height() - $("#flags-all").outerHeight(true) - $("#panel").outerHeight(true));

        $("#program-all .info-container").height((height - $("#panel").outerHeight(true))/3 - $("#program-all").outerHeight(true) +  $("#program-all .info-container").height());
        $("#data-all .info-container").height(height - $("#data-all").outerHeight(true) + $("#data-all .info-container").height() - $("#program-all").outerHeight(true) - $("#panel").outerHeight(true));
        editorManager.resize();
    }
                                
    function _setLayoutListeners() {
       $(window).resize(function(e) {
            if (e.target == window) {
                _resizeLayout();
            }
        });

        $(".status-container").resizable({
            resize: function(e, ui) {
                $("#editor-container").width($(".status-container").width());
            },
            stop: function(e, ui) {
                editorManager.resize();
            }
        });
        $("#editor-container").resizable({
            resize: function(e, ui) {
                $(".status-container").width($("#editor-container").width());
            },
            stop: function(e, ui) {
                editorManager.resize();
            }
        });                     
        $("#right1 .info-container").resizable({
            resize: function(e, ui) {
                var w = ui.element.width();
                $("#right1 .info-container").each(function() {
                    $(this).width(w);
                });
            },
        });
        $("#right2 .info-container").resizable({
            resize: function(e, ui) {
                var w = ui.element.width();
                $("#right2 .info-container").each(function() {
                    $(this).width(w);
                });
            }
        });
    }
    return {
        init: init,
    }
});
