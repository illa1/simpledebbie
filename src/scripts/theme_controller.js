define([
    "jquery",

    "editor_manager",
    "settings_manager",
], function($, editorManager, settingsManager) {
    var value = "light";
    settingsManager.register(_change);
    
    function _change(name, value_) {
        if (name == "Theme") {
            value = value_;
            _render();
        }
    }

	function _render() {
		if (value == "dark") {
			$("body").removeClass("light-theme");
			$("body").addClass("dark-theme");
			editorManager.setTheme("dark");
		} else {
			$("body").removeClass("dark-theme");
			$("body").addClass("light-theme");
			editorManager.setTheme("light");
		}
	}
});
