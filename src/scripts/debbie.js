define([
    "layout_manager",
    "processor_monitor",
    "file_manager",
    "editor_manager",
    "settings_manager",
    "buttons_status",
    "theme_controller",
], function(layoutManager, processorMonitor, fileManager,
    editorManager, settingsManager, buttonsStatus, themeController) {
    
    var processorList = [];
        processorNameList = [];
                
    function addProcessor(module) {
        processorList.push(module);
        processorNameList.push(module.getName());
    }

    function start() {
        processor = processorList[0];
        editorManager.init(processor.getEditorMode());
        settingsManager.init(processorNameList);
        fileManager.init();
        processorMonitor.init(processor);

        buttonsStatus.render();
        layoutManager.init();

        settingsManager.register(_changeProcessor);
    }

    function _changeProcessor(name, value) {
        var processor;
        if (name == "Processor") {
            processor = processorList[processorNameList.indexOf(value)];
            processorMonitor.setProcessor(processor);
            assembleManager.setProcessor(processor);
        }
    }

    return {
        addProcessor: addProcessor,
        start: start,
    }
});
