define([
    "jquery",
    "underscore",
    
    "edit_handler",

    "text!templates/flags.html",
], function($, _, EditHandler, flagsTemplate) {

    var flagsView = function(getValues, setValue) {
        var divName = '#flags',
            editHandler = new EditHandler(divName, _afterEdit);
        
        function _afterEdit(el) {
            var name = el.parent().attr('id').substr(1),
                value;
                    
            setValue(name, parseInt(el.val(),10));
            getValues().forEach( function (f) {
                if (f.name == name) {
                    value = f.value;
                }
            });
            return value;
        }

        function render() {
            editHandler.endEdit();
            $(divName).html(_.template(flagsTemplate, {flags: getValues()}));
            $(divName + ' .val').dblclick(function() {
                var index = $(divName + ' .val').index(this);
                if (index >= 0) {
                    editHandler.startEdit(index);
                }
            });
        }

        function change(changes) {
            if (changes.length <= 0) return;

            $(divName + ' .val').removeClass('last-changed');

            changes.forEach(function(c) {
                $(divName + ' #F' + c.index).addClass('last-changed');
                $(divName + ' #F' + c.index).html(c.value);                
            });
        }

        return {
            change: change,
            render: render,
        }
    };

    return flagsView;
});
