define([
    "jquery",
], function($) {

    var compiled = false,
        running = false,
        ableToBack = false;

    function render() {
        if (compiled == true && running == false) {
            setEnabled('#rewind');
            setEnabled('#next');
            setEnabled('#run');
        } else {
            setDisabled('#rewind');
            setDisabled('#next');
            setDisabled('#run');
        }

        if (ableToBack == true && running == false) {
            setEnabled('#back');
        } else {
            setDisabled('#back');
        }

        if (running == true) {
            setDisabled('#assemble');
            setEnabled('#run');
            $("#run").removeClass("glyphicon-forward");
            $("#run").addClass("glyphicon-stop");
        } else {
            setEnabled('#assemble');
            $("#run").removeClass("glyphicon-stop");
            $("#run").addClass("glyphicon-forward");
        }
    }

    function setEnabled(button) {
        $(button).removeClass('disabled-button');
        $(button).addClass('my-button');
    }

    function setDisabled(button) {
        $(button).removeClass('my-button');
        $(button).addClass('disabled-button');
    }

    function setCompiled(compiled_) {
        compiled = compiled_;
        render();
    }

    function setRunning(running_) {
        running = running_;
        render();
    }

    function setAbleToBack(ableToBack_) {
        ableToBack = ableToBack_; 
        render();
    }

    function getCompiled(compiled_) {
        return compiled;
    }

    function getRunning() {
        return running;
    }

    function getAbleToBack() {
        return ableToBack;
    }
    
    return {
        getCompiled: getCompiled,
        setCompiled: setCompiled,
        getAbleToBack: getAbleToBack,
        setAbleToBack: setAbleToBack,
        getRunning: getRunning,
        setRunning: setRunning,
        render: render,
    };
});
