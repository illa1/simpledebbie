define([
    "jquery",
    "underscore",
    "bootstrap",

    "flags_view",
    "registers_view",
    "data_view",

    "editor_manager",
    "buttons_status",

    "text!templates/status.html",
    "text!templates/errors.html",
], function ($, _, bootstrap, FlagsView, RegistersView, DataView,
    editorManager, buttonsStatus, statusTemplate, errorsTemplate) {

    var processor,
        status = $("#status-text"),
        flagsView,
        registersView,
        dataView,
        programView,
        currentProcessorState,
        lastProcessorState,
        changesList,
        maxListLength = 100,
        timeout = 10,
        memoryViewMaxSize = 1024,
        programViewMaxSize = 2048,
        timer,
        assembledProgram,
        breakpointList;

    function _runOrStop() {
        if (buttonsStatus.getCompiled() == false) return;
        if (buttonsStatus.getRunning() == true) {
            clearTimeout(timer);
            buttonsStatus.setRunning(false);
        } else {
            buttonsStatus.setRunning(true);
            timer = setTimeout(_nextInRun, timeout);
        }
    }

    function _nextInRun() {
        if (processor.next() == true) {
            buttonsStatus.setAbleToBack(true);
            _lookForChanges();
            _showProcessorState(false, false, changesList[changesList.length-1]);
            var line = processor.whatLine(currentProcessorState.counter) - 1;
            if (breakpointList.indexOf(line) < 0 ) {
                timer = setTimeout(_nextInRun, timeout);
            } else {
                buttonsStatus.setRunning(false);
            }
        } else {
            $("#bad-opcode-modal").modal("show");
            buttonsStatus.setRunning(false);
        }
    }

    function _getBreakpoints() {
        return breakpointList;
    }

    function _toggleBreakpoint(line) {
        if (breakpointList.indexOf(line) < 0) {
            breakpointList.push(line);
        } else {
            breakpointList.splice(breakpointList.indexOf(line), 1);
        }
    }

    function setProcessor(processor_) {
        processor = processor_;
        processor.reset();

        flagsView = new FlagsView(processor.getFlags, processor.setFlag);
        registersView = new RegistersView(processor.getRegisters, processor.setRegister);
        dataView = new DataView("MEMORY", "#data-top", "#data", processor.getMemoryChoices(),
                                processor.getMemory, processor.setMemory, memoryViewMaxSize),
        programView = new DataView("PROGRAM", "#program-top", "#program",
                            [{name: "program", start: 0, end: processor.getProgramMaxSize(), delta: 0}],
                            processor.getProgramMemory, processor.setProgramMemory, programViewMaxSize);

        currentProcessorState = _processorState();

        _showProcessorState(true, false);
    }

    function _clearChanges() {
        changesList = [];
        currentProcessorState = _processorState();
    }

    function init(processor_) {
        breakpointList = [];
        editorManager.setToggleBreakpoint(_toggleBreakpoint);

        $("#rewind").click(_rewind);
        $("#back").click(_back);
        $("#next").click(_next);
        $("#run").click(_runOrStop);
        $("#assemble").click(_assemble);

        setProcessor(processor_);
    }

    function _rewind() {
        if (buttonsStatus.getCompiled() == false || buttonsStatus.getRunning() == true) return;

        _loadProgram();
    }

    function _back() {
        var lastChanges = [];
                     
        if (buttonsStatus.getAbleToBack() == false || buttonsStatus.getRunning() == true) return;

        if (changesList.length > 0) {
            lastChanges = changesList.pop();
            if (changesList.length <= 1) {
                buttonsStatus.setAbleToBack(false);
            }
        }
        
        processor.setProgramCounter(lastChanges.counter.old);
        processor.setCycles(lastChanges.cycles.old);
        
        lastChanges.flags.forEach( function (f) {
            processor.setFlag(f.index, f.old);            
        });
        lastChanges.registers.forEach( function (r) {
            processor.setRegister(r.index, r.old);            
        });
        lastChanges.memory.forEach( function (m) {
            processor.setMemory(m.index, m.old);            
        });
        
        currentProcessorState = _processorState();
        _showProcessorState(false, true, lastChanges);
    }

    function _copyArray(array) {
        var result = [],
            max = array.length;

        for (var i = 0; i < max; i += 1) {
            result.push(_.clone(array[i]));
        }
        return result;
    }

    function _processorState() {
        var state = {};
        state.counter = processor.getProgramCounter();
        state.counterIndex = processor.getProgramCounterIndex();
        state.cycles = processor.getCycles();
        state.flags = _copyArray(processor.getFlags());
        state.registers = _copyArray(processor.getRegisters());
        state.memory = _copyArray(processor.getMemory());
        return state;
    }

    function _changesInList(oldList, currentList, names) {
        var max = oldList.length,
            names = names == undefined ? false : names,
            i,
            result = [];
            
        for (i = 0; i < max; i += 1) {
            if (names == true && oldList[i].value != currentList[i].value) {
                result.push({index: currentList[i].name, old: oldList[i].value, current: currentList[i].value});
            }
            if (names == false && oldList[i] != currentList[i]){
                result.push({index: i, old: oldList[i], current: currentList[i]});
            }
        }
        return result;
    }

    function _lookForChanges() {
        var changes = {};
        
        lastProcessorState = currentProcessorState;
        currentProcessorState = _processorState();
        if (currentProcessorState.counter != lastProcessorState.counter) {
            changes.counter = {old: lastProcessorState.counter, current: currentProcessorState.counter};
        }
        if (currentProcessorState.counterIndex != lastProcessorState.counterIndex) {
            changes.counterIndex = {old: lastProcessorState.counterIndex, current: currentProcessorState.counterIndex};
        }
        if (currentProcessorState.cycles != lastProcessorState.cycles) {
            changes.cycles = {old: lastProcessorState.cycles, current: currentProcessorState.cycles};
        }
        changes.flags = _changesInList(lastProcessorState.flags, currentProcessorState.flags, true );
        changes.registers = _changesInList(lastProcessorState.registers, currentProcessorState.registers, true );
        changes.memory = _changesInList(lastProcessorState.memory, currentProcessorState.memory);
  
        changesList.push(changes);
        if (changesList.length > maxListLength) {
            changesList.shift();
        }
    }

    function _showProcessorState(sort, back, lastChanges) {
        var line = processor.whatLine(currentProcessorState.counter),
            value = back == false ? "current" : "old",
            objectCopy = function (o) { return {index: o.index, value: o[value]}; };

        status.html(_.template(statusTemplate, {counter: currentProcessorState.counter, cycles: currentProcessorState.cycles}));
        
        if (lastChanges == undefined) {
            flagsView.render();
            registersView.render(sort);
            dataView.render();
            programView.render();
        } else {
            flagsView.change(lastChanges.flags.map(objectCopy));
            registersView.change(lastChanges.registers.map(objectCopy));
            dataView.change(lastChanges.memory.map(objectCopy));
        }

        if (buttonsStatus.getCompiled() == true) {
            editorManager.highlightLine(line);
            programView.highlight(currentProcessorState.counterIndex);
        }
    }
    
    function _next() {
        var lastChanges;
        if (buttonsStatus.getCompiled() == false || buttonsStatus.getRunning() == true) return;
        if (processor.next() == true) {
            buttonsStatus.setAbleToBack(true);
            _lookForChanges();
            _showProcessorState(false, false, changesList[changesList.length-1]);
        } else {
            $("#bad-opcode-modal").modal("show");
        }
    }

    function _assemble() {
        if (buttonsStatus.getRunning() == true) return;
        var text = editorManager.getText();
            program = [];
                
        editorManager.clearErrorsAndHighlight();
        buttonsStatus.setAbleToBack(false);

        assembledProgram = processor.assemble(text);
        if (processor.getErrors().length == 0) {
            buttonsStatus.setCompiled(true);
            _loadProgram();
        } else {
            $("#status-text").html(_.template(errorsTemplate, { errors: processor.getErrors() }));
            editorManager.setErrors(processor.getErrors().map(function (e) { return e.line; }));
            buttonsStatus.setCompiled(false);
        }
    }
    
    function _loadProgram() {
        processor.reset();
        processor.load(assembledProgram);
        _clearChanges();
        _lookForChanges();
        _showProcessorState(false, false);
    }

    return {
        init: init,
        setProcessor: setProcessor,
    }
});
