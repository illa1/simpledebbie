define(['processors/at90/at90_opcodes'], function(opcodeTranslator) {

    var counter, //w slowach
        timer,
        flags,
        memory,
        program,
        xl = 26,
        xh = 27,
        yl = 28,
        yh = 29,
        zl = 30,
        zh = 31,
        spl = 61,
        sph = 62,
        sreg = 63,
        commands = {
            adc: function(reg1, reg2) {
                var rd = memory[reg1],
                    rr = memory[reg2],
                    r = (rr + rd + flags.c) % 256;
                _flagH(_bit(rd,3) * _bit(rr,3) | _bit(rr,3) * _not(_bit(r,3)) | _not(_bit(r,3)) * _bit(rd,3));
                _setFlagsSNZ(r);
                _flagV(_bit(rd,7) * _bit(rr,7) * _not(_bit(r,7)) | _not(_bit(rd,7)) * _not(_bit(rr,7)) * _bit(r,7));
                _flagC(_bit(rd,7) * _bit(rr,7) | _bit(rr,7) * _not(_bit(r,7)) | _not(_bit(r,7)) * _bit(rd,7));
                memory[reg1] = r;
                counter += 1;
                timer += 1;
            },
            add: function(reg1, reg2) {
                var rd = memory[reg1],
                    rr = memory[reg2],
                    r = (rr + rd) % 256;
                //tak jak adc
                _flagH(_bit(rd,3) * _bit(rr,3) | _bit(rr,3) * _not(_bit(r,3)) | _not(_bit(r,3)) * _bit(rd,3));
                _setFlagsSNZ(r);
                _flagV(_bit(rd,7) * _bit(rr,7) * _not(_bit(r,7)) | _not(_bit(rd,7)) * _not(_bit(rr,7)) * _bit(r,7));
                _flagC(_bit(rd,7) * _bit(rr,7) | _bit(rr,7) * _not(_bit(r,7)) | _not(_bit(r,7)) * _bit(rd,7));
                memory[reg1] = r;
                counter += 1;
                timer += 1;
            },
            adiw : function(reg1, k) {
                var rd = memory[reg1] + (memory[reg1+1] << 8),
                    r = (rd + k) % (1 << 16);
                console.log(rd);
                _flagS(flags.n ^ flags.v);
                _flagV(_not(_bit(rd,15)) * _bit(r,15));
                _flagN(_bit(r,15));
                _flagZ(r == 0 ? 1 : 0);
                _flagC(_not(_bit(r,15)) * _bit(rd, 15));
                memory[reg1] = r % 256;
                memory[reg1+1] = r >> 8;
                counter += 1;
                timer += 2;
            },
            and: function(reg1, reg2) { //OK
                var result = memory[reg1] & memory[reg2];
                _setFlagsSNZ(result);
                _flagV(0);
                memory[reg1] = result;
                counter += 1;
                timer += 1;
            },
            andi: function(reg1, k) {
                var result = memory[reg1] & k;
                _setFlagsSNZ(result);
                _flagV(0);
                memory[reg1] = result;
                counter += 1;
                timer += 1;
            },
            asr: function(reg1) { //OK ??
                var val = memory[reg1];
                memory[reg1] = (val & 128) + (val >> 1);
                _setFlagsSNZ(memory[reg1]);
                _flagC(val % 2);
                _flagV(flags.n ^ flags.c);
                counter += 1;
                timer += 1;
            },
            bclr: function(bit) { //OK
                switch(bit) {
                case 0:
                    _flagC(0);
                    break;
                case 1:
                    _flagZ(0);
                    break;
                case 2:
                    _flagN(0);
                    break;
                case 3:
                    _flagV(0);
                    break;
                case 4:
                    _flagS(0);
                    break;
                case 5:
                    _flagH(0);
                    break;
                case 6:
                    _flagT(0);
                    break;
                case 7:
                    _flagI(0);
                    break;
                }
                counter += 1;
                timer += 1;
            },
            bld: function(reg1, bit) {
                memory[reg1] &= (255 - (1 << bit));
                memory[reg1] |= (flags.t << bit);
                counter += 1;
                timer += 1;
            },
            brbc: function(bit, delta) { //OK
                var flagsNumbers = [
                    flags.c, flags.z, flags.n, flags.v, flags.s, flags.h, flags.t, flags.i
                ];
                if (flagsNumbers[bit] == 0) {
                    counter += 1 + delta;
                    timer += 2;
                } else {
                    counter += 1;
                    timer += 1;
                }
            },
            brbs: function(bit, delta) { //OK
                var flagsNumbers = [
                    flags.c, flags.z, flags.n, flags.v, flags.s, flags.h, flags.t, flags.i
                ];
                if (flagsNumbers[bit] == 1) {
                    counter += 1 + delta;
                    timer += 2;
                } else {
                    counter += 1;
                    timer += 1;
                }
            },
            brcc: function(delta) { //OK
                this.brbc(0, delta);
            },
            brcs: function(delta) { //OK
                this.brbs(0, delta);
            },
            break: function() { //OK
                counter += 1;
                timer += 1;
            },
            breq: function(delta) { //OK
                this.brbs(1, delta);
            },
            brge: function(delta) { //OK
                this.brbc(4, delta);
            },
            brhc: function(delta) { //OK
                this.brbc(5, delta);
            },
            brhs: function(delta) { //OK
                this.brbs(4, delta);
            },
            brid: function(delta) { //OK
                this.brbc(7, delta);
            },
            brie: function(delta) { //OK
                this.brbs(7, delta);
            },
            brlo: function(delta) { //OK
                this.brbs(0, delta);
            },
            brlt: function(delta) { //OK
                this.brbs(4, delta);
            },
            brmi: function(delta) { //OK
                this.brbs(2, delta);
            },
            brne: function(delta) { //OK
                this.brbs(1, delta);
            },
            brpl: function(delta) { //OK
                this.brbc(2, delta);
            },
            brsh: function(delta) { //OK
                this.brbc(0, delta);
            },
            brtc: function(delta) { //OK
                this.brbc(6, delta);
            },
            brts: function(delta) { //OK
                this.brbs(6, delta);
            },
            brvc: function(delta) { //OK
                this.brbc(3, delta);
            },
            brvs: function(k) { //OK
                this.brbs(3, k);
            },
            bset: function(bit) { //OK
                switch(bit) {
                case 0:
                    _flagC(1);
                    break;
                case 1:
                    _flagZ(1);
                    break;
                case 2:
                    _flagN(1);
                    break;
                case 3:
                    _flagV(1);
                    break;
                case 4:
                    _flagS(1);
                    break;
                case 5:
                    _flagH(1);
                    break;
                case 6:
                    _flagT(1);
                    break;
                case 7:
                    _flagI(1);
                    break;
                }
                counter += 1;
                timer += 1;
            },
            bst: function(reg1, bit) { //OK
                _flagT(_bit(memory[reg1], bit));
                counter += 1;
                timer += 1;
            },
            call: function(address) { //OK
                _stackPush((counter+2) % 256); // +2 ?
                _stackPush((counter+2) >> 8); // +2 ?
                counter = address;
                timer += 4; //depends on device 3,4 or 5
            },
            cbi: function(regIO, bit) {
                memory[32+regIO] &= (255 - (1 << bit));
                counter += 1;
                timer += 2;
            },
            cbr: function(reg1, k) {
                this.andi(reg1, 255-k);
            },
            clc: function() { //OK
                _flagC(0);
                counter += 1;
                timer += 1;
            },
            clh: function() { //OK
                _flagH(0);
                counter += 1;
                timer += 1;
            },
            cli: function() { //OK
                _flagI(0);
                counter += 1;
                timer += 1;
            },
            cln: function() { //OK
                _flagN(0);
                counter += 1;
                timer += 1;
            },
            clr: function(reg1) { //OK
                memory[reg1] = 0;
                _flagS(0);
                _flagV(0);
                _flagN(0);
                _flagZ(1);
                counter += 1;
                timer += 1;
            },
            cls: function() { //OK
                _flagS(0);
                counter += 1;
                timer += 1;
            },
            clt: function() { //OK
                _flagT(0);
                counter += 1;
                timer += 1;
            },
            clv: function() { //OK
                _flagT(0);
                counter += 1;
                timer += 1;
            },
            clz: function() { //OK
                _flagZ(0);
                counter += 1;
                timer += 1;
            },
            com: function(reg1) {
                var r = 255 - memory[reg1];
                _flagS(flags.n ^ flags.v);
                _flagV(0);
                _flagN(_bit(r, 7));
                _flagZ(r == 0 ? 1 : 0);
                _flagC(1);
                memory[reg1] = r;
                counter += 1;
                timer += 1;
            },
            cp: function(reg1, reg2) {
                var rd = memory[reg1],
                    rr = memory[reg2],
                    r = _subtract(rd, rr);
                _flagH(_not(_bit(rd,3)) * _bit(rr,3) | _bit(rr,3) * _bit(r,3) | _bit(r,3) * _not(_bit(rd,3)));
                _setFlagsSNZ(r);
                _flagV(_bit(rd,7) * _not(_bit(rr,7)) * _not(_bit(r,7)) | _not(_bit(rd,7)) * _bit(rr,7) * _bit(r,7));
                _flagC(_not(_bit(rd,7)) * _bit(rr,7) | _bit(rr,7) * _bit(r,7) | _bit(r,7) * _not(_bit(rd,7)));
                counter += 1;
                timer += 1;
            },
            cpc: function(reg1, reg2) {
                var rd = memory[reg1],
                    rr = memory[reg2],
                    r = _subtract(_subtract(rd, rr), flags.c);

                _flagH(_not(_bit(rd,3)) * _bit(rr,3) | _bit(rr,3) * _bit(r,3) | _bit(r,3) * _not(_bit(rd,3)));
                _flagS(flags.n ^ flags.v);
                _flagZ(r == 0 && flags.z == 1 ? 1 : 0);
                _flagN(_bit(r,7));
                _flagV(_bit(rd,7) * _not(_bit(rr,7)) * _not(_bit(r,7)) | _not(_bit(rd,7)) * _bit(rr,7) * _bit(r,7));
                _flagC(_not(_bit(rd,7)) * _bit(rr,7) | _bit(rr,7) * _bit(r,7) | _bit(r,7) * _not(_bit(rd,7)));
                counter += 1;
                timer += 1;
            },
            cpi: function(reg1, k) {
                var rd = memory[reg1],
                    r = _subtract(rd, k);

                _flagH(_not(_bit(rd,3)) * _bit(k,3) | _bit(k,3) * _bit(r,3) | _bit(r,3) * _not(_bit(rd,3)));
                _setFlagsSNZ(r);
                _flagV(_bit(rd,7) * _not(_bit(k,7)) * _not(_bit(r,7)) | _not(_bit(rd,7)) * _bit(k,7) * _bit(r,7));
                _flagC(_not(_bit(rd,7)) * _bit(k,7) | _bit(k,7) * _bit(r,7) | _bit(r,7) * _not(_bit(rd,7)));
                counter += 1;
                timer += 1;
            },
            cpse: function(reg1, reg2) { // trzeba znac dlugosc nastepnej komendy
                var delta = 0;
                if (memory[reg1] == memory[reg2]) {
                    delta = _nextCommandLength(1);
                }
                counter += 1 + delta;
                timer += 1 + delta;
            },
            dec: function(reg1) {
                var result = memory[reg1] - 1;
                if (result == -1) {
                    result = 255;
                }
                memory[reg1] = result;
                _setFlagsSNZ(result);
                _flagV(result == 127 ? 1 : 0);
                counter += 1;
                timer += 1;
            },
            des: function(k) {
                // TO DO
                counter += 1;
                timer += 1;
            },
            eor: function(reg1, reg2) {
                var result = memory[reg1] ^ memory[reg2];
                _setFlagsSNZ(result);
                _flagV(0);
                memory[reg1] = result;
                counter += 1;
                timer += 1;
            },

            in: function(reg1, regIO) {
                //nie zawsze
                memory[reg1] = memory[regIO + 32];
                counter += 1;
                timer += 1;
            },
            inc: function(reg1) {
                var result = (memory[reg1] + 1) % 256;
                memory[reg1] = result;
                _setFlagsSNZ(result);
                _flagV(result == 128 ? 1 : 0);
                counter += 1;
                timer += 1;
            },
            jmp: function(address) { //OK
                counter = address;
                timer += 3;
            },
            lac_x: function(reg1) {
                var address = memory[zl] + (memory[zh] << 8);
                memory[address] = memory[reg1] & (255 - memory[address]);
                counter += 1;
                timer += 1;
            },
            las_x: function(reg1) {
                var address = memory[zl] + (memory[zh] << 8);
                memory[address] = memory[reg1] | memory[address];
                memory[reg1] = memory[address];
                counter += 1;
                timer += 1;
            },
            lat_x: function(reg1) {
                var address = memory[zl] + (memory[zh] << 8);
                memory[address] = memory[reg1] ^ memory[address];
                memory[reg1] = memory[address];
                counter += 1;
                timer += 1;
            },
            ld_x: function(reg1) {
                var regAddress = 26,
                    address = memory[regAddress] + (memory[regAddress+1] << 8);
                memory[reg1] = memory[address];
                counter += 1;
                timer += 1;
            },
            ld_y: function(reg1) {
                var regAddress = 28,
                    address = memory[regAddress] + (memory[regAddress+1] << 8);
                memory[reg1] = memory[address];
                counter += 1;
                timer += 1;
            },
            ld_z: function(reg1) {
                var regAddress = 30,
                    address = memory[regAddress] + (memory[regAddress+1] << 8);
                memory[reg1] = memory[address];
                counter += 1;
                timer += 1;
            },
            ld_xp: function(reg1) {
                var regAddress = 26,
                    address = memory[regAddress] + (memory[regAddress+1] << 8);
                memory[reg1] = memory[address];
                address = (address + 1) % (1 << 16);
                memory[regAddress] = address % 256;
                memory[regAddress+1] = address >> 8;
                counter += 1;
                timer += 2;
            },
            ld_yp: function(reg1) {
                var regAddress = 28,
                    address = memory[regAddress] + (memory[regAddress+1] << 8);
                memory[reg1] = memory[address];
                address = (address + 1) % (1 << 16);
                memory[regAddress] = address % 256;
                memory[regAddress+1] = address >> 8;
                counter += 1;
                timer += 2;
            },
            ld_zp: function(reg1) {
                var regAddress = 30,
                    address = memory[regAddress] + (memory[regAddress+1] << 8);
                memory[reg1] = memory[address];
                address = (address + 1) % (1 << 16);
                memory[regAddress] = address % 256;
                memory[regAddress+1] = address >> 8;
                counter += 1;
                timer += 2;
            },
            ld_xm: function(reg1) {
                var regAddress = 26,
                    address = memory[regAddress] + (memory[regAddress+1] << 8);
                address = address - 1;
                if (address < 0) address = (1 << 16) - 1;
                memory[regAddress] = address % 256;
                memory[regAddress+1] = address >> 8;
                memory[reg1] = memory[address];
                counter += 1;
                timer += 3;
            },
            ld_xm: function(reg1) {
                var regAddress = 28,
                    address = memory[regAddress] + (memory[regAddress+1] << 8);
                address = address - 1;
                if (address < 0) address = (1 << 16) - 1;
                memory[regAddress] = address % 256;
                memory[regAddress+1] = address >> 8;
                memory[reg1] = memory[address];
                counter += 1;
                timer += 3;
            },
            ld_xm: function(reg1) {
                var regAddress = 30,
                    address = memory[regAddress] + (memory[regAddress+1] << 8);
                address = address - 1;
                if (address < 0) address = (1 << 16) - 1;
                memory[regAddress] = address % 256;
                memory[regAddress+1] = address >> 8;
                memory[reg1] = memory[address];
                counter += 1;
                timer += 3;
            },

            ldi: function(reg1, value) { //OK
                memory[reg1] = value;
                counter += 1;
                timer += 1;
            },
            lds: function(reg1, address) {
                memory[reg1] = memory[address];
                counter += 2;
                timer += 2;
            },
            lpm: function(reg1) {
                var address = memory[30] + (memory[31] << 8);
                memory[reg1] = program[address];
                counter += 1;
                timer += 3;
            },
            lpm_z: function(reg1) {
                var address = memory[30] + (memory[31] << 8);
                memory[reg1] = program[address];
                counter += 1;
                timer += 3;
            },
            lpm_zp: function(reg1) {
                var address = memory[30] + (memory[31] << 8);
                memory[reg1] = program[address];
                address = (address + 1) % (1 << 16);
                memory[30] = address % 256;
                memory[31] = address >> 8;
                counter += 1;
                timer += 3;
            },
            lsl: function(reg1) {
                var rd = memory[reg1],
                    r;
                _flagC(_bit(rd, 7));
                r = (rd << 1) % 256;
                _flagH(_bit(rd, 3));
                _setFlagsSNZ(r);
                _flagV(flags.c ^ flags.n);
                memory[reg1] = r;
                counter += 1;
                timer += 1;
            },
            lsr: function(reg1) {
                var rd = memory[reg1],
                    r;
                _flagC(rd % 2);
                r = rd >> 1;
                _flagS(flags.n ^ flags.v);
                _flagN(0);
                _flagV(flags.c ^ flags.n);
                _flagZ(r == 0 ? 1 : 0);
                memory[reg1] = r;
                counter += 1;
                timer += 1;
            },
            mov: function(reg1, reg2) {
                memory[reg1] = memory[reg2];
                counter += 1;
                timer += 1;
            },
            movw: function(reg1, reg2) {
                memory[reg1] = memory[reg2];
                memory[reg1+1] = memory[reg2+1];
                counter += 1;
                timer += 1;
            },
            mul: function(reg1, reg2) {
                var r = memory[reg1] * memory[reg2];
                _flagC(_bit(r, 15));
                _flagZ(r == 0 ? 1 : 0);
                memory[0] = r % 256;
                memory[1] = r >> 8;
                counter += 1;
                timer += 2;
            },
            muls: function(reg1, reg2) {
                var rd = memory[reg1] < 128 ? memory[reg1] : memory[reg1] - 256,
                    rr = memory[reg2] < 128 ? memory[reg2] : memory[reg2] - 256,
                    r = rd * rr;
                if (r < 0) {
                    r = ((1 << 16) + r) % (1 << 16);
                }
                _flagC(_bit(r, 15));
                _flagZ(r == 0 ? 1 : 0);
                memory[0] = r % 256;
                memory[1] = r >> 8;
                counter += 1;
                timer += 2;
            },
            mulsu: function(reg1, reg2) {
                var rd = memory[reg1] < 128 ? memory[reg1] : memory[reg1] - 256,
                    rr = memory[reg2],
                    r = rd * rr;

                if (r < 0) {
                    r = ((1 << 16) + r) % (1 << 16);
                }
                _flagC(_bit(r, 15));
                _flagZ(r == 0 ? 1 : 0);
                memory[0] = r % 256;
                memory[1] = r >> 8;
                counter += 1;
                timer += 2;
            },
            neg: function(reg1) {
                var rd = memory[reg1],
                    r = (256 - rd) % 256;
                _flagH(_bit(r, 3) | _bit(rd, 3));
                _setFlagsSNZ(r);
                _flagV(r == 128 ? 1 : 0);
                _flagC(r == 0 ? 0 : 1);
                memory[reg1] = r;
                counter += 1;
                timer += 1;
            },
            nop: function() {
                counter += 1;
                timer += 1;
            },
            or: function(reg1, reg2) {
                var result = memory[reg1] | memory[reg2];
                _setFlagsSNZ(result);
                _flagV(0);
                memory[reg1] = result;
                counter += 1;
                timer += 1;
            },
            ori: function(reg1, k) {
                var result = memory[reg1] | k;
                _setFlagsSNZ(result);
                _flagV(0);
                memory[reg1] = result;
                counter += 1;
                timer += 1;
            },
            out: function(regIO, reg1) {
                //nie zawsze
                memory[regIO + 32] = memory[reg1];
                counter += 1;
                timer += 1;
            },
            pop: function(reg1) { //OK
                memory[reg1] = _stackPop();
                counter += 1;
                timer += 2;
            },
            push: function(reg1) { //OK
                _stackPush(memory[reg1]);
                counter += 1;
                timer += 2;
            },
            rcall: function(delta) { //OK
                _stackPush((counter+1) % 256); // +2 ?
                _stackPush((counter+1) >> 8); // +2 ?
                counter += delta + 1; //+1??
                timer += 3; //4,5
            },
            ret: function() {
                counter = _stackPop() << 8;
                counter += _stackPop();
                timer += 4;
            },
            reti: function() {
                counter = _stackPop() << 8;
                counter += _stackPop();
                _flagI(1);
                timer += 4;
            },
            rjmp: function(delta) {
                counter += delta + 1; // znow +1 ?
                timer += 2;
            },
            rol: function(reg1) {
                var rd = memory[reg1],
                    r;
                _flagC(_bit(rd, 7));
                r = ((rd << 1) + flags.c) % 256;
                _flagH(_bit(rd, 3));
                _setFlagsSNZ(r);
                _flagV(flags.c ^ flags.n);
                memory[reg1] = r;
                counter += 1;
                timer += 1;
            },
            ror: function(reg1) {
                var rd = memory[reg1],
                    r;
                _flagC(rd % 2);
                r = (rd >> 1) + (flags.c << 7);
                _setFlagsSNZ(r);
                _flagV(flags.c ^ flags.n);
                memory[reg1] = r;
                counter += 1;
                timer += 1;
            },
            sbc: function(reg1, reg2) {
                var rd = memory[reg1],
                    rr = memory[reg2],
                    r = _subtract(_subtract(rd, rr), flags.c);
                _flagH(_not(_bit(rd,3)) * _bit(rr,3) | _bit(rr,3) * _bit(r,3) | _bit(r,3) * _not(_bit(rd,3)));
                _flagS(flags.n ^ flags.v);
                _flagV(bit(rd,7) * _not(_bit(rr,7)) * _not(_bit(r,7)) | _not(_bit(rd,7)) * _bit(rr,7) * _bit(r,7));
                _flagN(_bit(r, 7));
                _flagZ(r == 0 && flags.z == 1 ? 1 : 0);
                _flagC(_not(_bit(rd,7)) * _bit(rr,7) | _bit(rr,7) * _bit(r,7) | _bit(r,7) * _not(_bit(rd,7)));
                memory[reg1] = r;
                counter += 1;
                timer += 1;
            },
            sbci: function(reg1, k) {
                var rd = memory[reg1],
                    r = _subtract(_subtract(rd, k), flags.c);
                _flagH(_not(_bit(rd,3)) * _bit(k,3) | _bit(k,3) * _bit(r,3) | _bit(r,3) * _not(_bit(rd,3)));
                _flagS(flags.n ^ flags.v);
                _flagV(_bit(rd,7) * _not(_bit(k,7)) * _not(_bit(r,7)) | _not(_bit(rd,7)) * _bit(k,7) * _bit(r,7));
                _flagN(_bit(r, 7));
                _flagZ(r == 0 && flags.z == 1 ? 1 : 0);
                _flagC(_not(_bit(rd,7)) * _bit(k,7) | _bit(k,7) * _bit(r,7) | _bit(r,7) * _not(_bit(rd,7)));
                memory[reg1] = r;
                counter += 1;
                timer += 1;
            },
            sbi: function(regIO, bit) {
                memory[32+regIO] |= (1 << bit);
                counter += 1;
                timer += 2;
            },
            sbic: function(regIO, bit) {
                var delta = 0;
                if (_bit(memory[regIO+32], bit) == 0) {
                    delta = _nextCommandLength(1);
                }
                counter += 1 + delta;
                timer += 1 + delta;
            },
            sbis: function(regIO, bit) {
                var delta = 0;
                if (_bit(memory[regIO+32], bit) == 1) {
                    delta = _nextCommandLength(1);
                }
                counter += 1 + delta;
                timer += 1 + delta;
            },
            sbiw: function(reg1, k) {
                var rd = memory[reg1] + (memory[reg1+1] << 8),
                    r = rd - k;
                if (r < 0) {
                    r = (1 << 16) + r;
                }
                _flagS(flags.n ^ flags.v);
                _flagV(_bit(rd, 15) * _not(_bit(r, 15)));
                _flagN(_bit(r, 15));
                _flagZ(r == 0 ? 1 : 0);
                _flagC(_bit(r, 15) * _not(_bit(rd, 15)));
                memory[reg1] = r % 256;
                memory[reg1+1] = r >> 8;
                counter += 1;
                timer += 2;
            },
            sbr: function(reg1, k) {
                this.ori(reg1, k);
            },
            sbrc: function(reg1, k) {  // trzeba znac dlugosc nastepnej komendy
                var delta = 0;
                if (_bit(memory[reg1], k) == 0) {
                    delta = _nextCommandLength(1);
                }
                counter += 1 + delta;
                timer += 1 + delta;
            },
            sbrs: function(reg1, k) {  // trzeba znac dlugosc nastepnej komendy
                var delta = 0;
                if (_bit(memory[reg1], k) == 1) {
                    delta = _nextCommandLength(1);
                }
                counter += 1 + delta;
                timer += 1 + delta;
            },
            sec: function() {
                _flagC(1);
                counter += 1;
                timer += 1;
            },
            seh: function() {
                _flagH(1);
                counter += 1;
                timer += 1;
            },
            sei: function() {
                _flagI(1);
                counter += 1;
                timer += 1;
            },
            sen: function() {
                _flagN(1);
                counter += 1;
                timer += 1;
            },
            ser: function(reg1) {
                memory[reg1] = 255;
            },
            ses: function() {
                _flagS(1);
                counter += 1;
                timer += 1;
            },
            set: function() {
                _flagT(1);
                counter += 1;
                timer += 1;
            },
            sev: function() { // jak zdrobnienie od Severus <3
                _flagV(1);
                counter += 1;
                timer += 1;
            },
            sez: function() {
                _flagZ(1);
                counter += 1;
                timer += 1;
            },
            sleep: function() { // jeszcze nie ok
                counter += 1;
                timer += 1;
            },
            st_x: function(reg1) {
                var regAddress = 26,
                    address = memory[regAddress] + (memory[regAddress+1] << 8);
                memory[address] = memory[reg1];
                counter += 1;
                timer += 1;
            },
            st_y: function(reg1) {
                var regAddress = 28,
                    address = memory[regAddress] + (memory[regAddress+1] << 8);
                memory[address] = memory[reg1];
                counter += 1;
                timer += 1;
            },
            st_z: function(reg1) {
                var regAddress = 30,
                    address = memory[regAddress] + (memory[regAddress+1] << 8);
                memory[address] = memory[reg1];
                counter += 1;
                timer += 1;
            },
            st_xp: function(reg1) {
                var regAddress = 26,
                    address = memory[regAddress] + (memory[regAddress+1] << 8);
                memory[address] = memory[reg1];
                address = (address + 1) % (1 << 16);
                memory[regAddress] = address % 256;
                memory[regAddress+1] = address >> 8;
                counter += 1;
                timer += 2;
            },
            st_yp: function(reg1) {
                var regAddress = 28,
                    address = memory[regAddress] + (memory[regAddress+1] << 8);
                memory[address] = memory[reg1];
                address = (address + 1) % (1 << 16);
                memory[regAddress] = address % 256;
                memory[regAddress+1] = address >> 8;
                counter += 1;
                timer += 2;
            },
            st_zp: function(reg1) {
                var regAddress = 30,
                    address = memory[regAddress] + (memory[regAddress+1] << 8);
                memory[address] = memory[reg1];
                address = (address + 1) % (1 << 16);
                memory[regAddress] = address % 256;
                memory[regAddress+1] = address >> 8;
                counter += 1;
                timer += 2;
            },
            st_xm: function(reg1) {
                var regAddress = 26,
                    address = memory[regAddress] + (memory[regAddress+1] << 8);
                address = address - 1;
                if (address < 0) address = (1 << 16) - 1;
                memory[regAddress] = address % 256;
                memory[regAddress+1] = address >> 8;
                memory[address] = memory[reg1];
                counter += 1;
                timer += 3;
            },
            st_ym: function(reg1) {
                var regAddress = 28,
                    address = memory[regAddress] + (memory[regAddress+1] << 8);
                address = address - 1;
                if (address < 0) address = (1 << 16) - 1;
                memory[regAddress] = address % 256;
                memory[regAddress+1] = address >> 8;
                memory[address] = memory[reg1];
                counter += 1;
                timer += 3;
            },
            st_zm: function(reg1) {
                var regAddress = 30,
                    address = memory[regAddress] + (memory[regAddress+1] << 8);
                address = address - 1;
                if (address < 0) address = (1 << 16) - 1;
                memory[regAddress] = address % 256;
                memory[regAddress+1] = address >> 8;
                memory[address] = memory[reg1];
                counter += 1;
                timer += 3;
            },

            sts: function(address, reg1) {
                memory[address] = memory[reg1];
                counter += 2;
                timer += 1;
            },
            sub: function(reg1, reg2) {
                var rd = memory[reg1],
                    rr = memory[reg2],
                    r = _subtract(rd, rr);
                _flagH(_not(_bit(rd,3)) * _bit(rr,3) | _bit(rr,3) * _bit(r,3) | _bit(r,3) * _not(_bit(rd,3)));
                _setFlagsSNZ(r);
                _flagV(_bit(rd,7) * _not(_bit(rr,7)) * _not(_bit(r,7)) | _not(_bit(rd,7)) * _bit(rr,7) * _bit(r,7));
                _flagC(_not(_bit(rd,7)) * _bit(rr,7) | _bit(rr,7) * _bit(r,7) | _bit(r,7) * _not(_bit(rd,7)));
                memory[reg1] = r;
                counter += 1;
                timer += 1;
            },
            subi: function(reg1, k) {
                var rd = memory[reg1],
                    r = _subtract(rd, k);
                _flagH(_not(_bit(rd,3)) * _bit(k,3) | _bit(k,3) * _bit(r,3) | _bit(r,3) * _not(_bit(rd,3)));
                _setFlagsSNZ(r);
                _flagV(_bit(rd,7) * _not(_bit(k,7)) * _not(_bit(r,7)) | _not(_bit(rd,7)) * _bit(k,7) * _bit(r,7));
                _flagC(_not(_bit(rd,7)) * _bit(k,7) | _bit(k,7) * _bit(r,7) | _bit(r,7) * _not(_bit(rd,7)));
                memory[reg1] = r;
                counter += 1;
                timer += 1;
            },
            swap: function(reg1) {
                var rd = memory[reg1];
                rd = ((rd >> 4) + (rd << 4)) % 256;
                memory[reg1] = rd;
                counter += 1;
                timer += 1;
            },
            tst: function(reg1) {
                _setFlagsSNZ(result);
                _flagV(0);
                counter += 1;
                timer += 1;
            },
            xch_z: function(reg1) {
                var regAddress = 30,
                    address = memory[regAddress] + (memory[regAddress+1] << 8),
                    tmp;
                tmp = memory[address];
                memory[address] = memory[reg1];
                memory[reg1] = tmp;
                counter += 1;
                timer += 1;
            },
        };

    function _subtract(n1, n2) {
        var result = n1 - n2;
        if (result < 0) {
            result = 256 + result;
        }
        return result;
    }

    function _setFlagsSNZ(result) {
        _flagS(flags.n ^ flags.v);
        _flagN(_bit(result, 7));
        _flagZ(result ? 0 : 1);
    }

    function _flagC(value) {
        var result = memory[32+sreg];
        flags.c = value;
        result = result & parseInt(11111110, 2);
        result = result | value;
        memory[32+sreg] = result;
    }

    function _flagZ(value) {
        var result = memory[32+sreg];
        flags.z = value;
        result = result & parseInt(11111101, 2);
        result = result | (value << 1);
        memory[32+sreg] = result;
    }

    function _flagN(value) {
        var result = memory[32+sreg];
        flags.n = value;
        result = result & parseInt(11111011, 2);
        result = result | (value << 2);
        memory[32+sreg] = result;
    }

    function _flagV(value) {
        var result = memory[32+sreg];
        flags.v = value;
        result = result & parseInt(11110111, 2);
        result = result | (value << 3);
        memory[32+sreg] = result;
    }

    function _flagS(value) {
        var result = memory[32+sreg];
        flags.s = value;
        result = result & parseInt(11101111, 2);
        result = result | (value << 4);
        memory[32+sreg] = result;
    }

    function _flagH(value) {
        var result = memory[32+sreg];
        flags.h = value;
        result = result & parseInt(11011111, 2);
        result = result | (value << 5);
        memory[32+sreg] = result;
    }

    function _flagT(value) {
        var result = memory[32+sreg];
        flags.t = value;
        result = result & parseInt(10111111, 2);
        result = result | (value << 6);
        memory[32+sreg] = result;
    }

    function _flagI(value) {
        var result = memory[32+sreg];
        flags.i = value;
        result = result & parseInt(01111111, 2);
        result = result | (value << 7);
        memory[32+sreg] = result;
    }

    function _not(n) {
        if (n == 1) return 0;
        return 1;
    }

    function _bitwiseNot8(n) {
        return (~ (n | 1 << 31) ) % 256;
    }

    function _stackPush(value) {
        var sp = memory[spl+32] + (memory[sph+32] << 8);
        memory[sp] = value;
        sp = sp - 1;
        if (sp == 0) {
            sp = (1 << 32) -1;
        }
        memory[spl+32] = sp % 256;
        memory[sph+32] = sp >> 8;
    }

    function _stackPop() {
        var sp = memory[spl+32] + (memory[sph+32] << 8);
        sp = (sp + 1) % (1 << 16);
        memory[spl+32] = sp % 256;
        memory[sph+32] = sp >> 8;
        return memory[sp];
    }

    function load(program_) {
        var i;
        for (i = 0; i < program_.length; i+=1 ) {
            program[i] = program_[i];
        }
    }

    function reset() {
        var maxMemory = 8448,
            maxProgram = 64*1024*2;
        timer = 0;
        counter = 0;
        memory = new Array(maxMemory);
        while (maxMemory) {
            maxMemory -= 1;
            memory[maxMemory] = 255;
        }
        program = new Array(maxProgram);
        while (maxProgram) {
            maxProgram -= 1;
            program[maxProgram] = 255;
        }
        flags = {i: 0, t: 0, h: 0, s: 0, v: 0, n: 0, z: 0, c: 0},
        memory[sreg+32] = 0;
    }

    function _nextCommandLength(delta) {
        var twoWords = ['call','jmp', 'lds', 'sts'],
            address = 2*(delta+counter);
            opcode2 = program.slice(address, address + 2),
            opcode4 = program.slice(address, address + 4),
            command = opcodeTranslator.fromOpcode(opcode2);

        if (command == null) {
            command = opcodeTranslator.fromOpcode(opcode4);
        }
        if (command == null) {
            return 0;
        }
        if (twoWords.indexOf(command.name) >= 0) {
            return 2;
        }
        return 1;
    }

    function next() {
        var bytes2 = program.slice(2*counter, 2*counter + 2),
            bytes4 = program.slice(2*counter, 2*counter + 4),
            command;
        command = opcodeTranslator.fromOpcode(bytes2);
        if (command == null) {
            command = opcodeTranslator.fromOpcode(bytes4);
            if (command == null) {
                return false;
            }
        }
        commands[command.name].apply(null, command.args);
        return true;
    }

    function _bit(value, n) {
        return (value >> n) % 2;
    }

    function getFlags() {
        var result = [];
        for (f in flags) {
            result.push({name: f, value: flags[f]});
        }
        return result;
    }

    function getProgramCounter() {
        return counter;
    }

    function getCycles() {
        return timer;
    }

    function getMemory() {
        return memory;
    }

    function getRegisters() {
        var result = [];
        for (var i = 0; i < 32; i+= 1) {
            result.push({value: memory[i], name: 'R'+i});
        }
        return result;
    }

    function getMemoryChoices() {
        var result = [];
        result.push({name: 'ram', start: 256, end : 8448, delta: 0});
        result.push({name: 'i/o', start: 0, end : 64, delta: 32});
        return result;
    }

    function getProgramMemory() {
        return program;
    }

    function setProgramCounter(value) {
        counter = value;
    }

    function setCycles(value) {
        timer = value;
    }

    function setFlag(name, value) {
        if (value < 0 || value > 1) {
            return;
        }
        switch(name) {
            case 'c' :
                _flagC(value);
                break;
            case 'n' :
                _flagN(value);
                break;
            case 'z' :
                _flagZ(value);
                break;
            case 's' :
                _flagS(value);
                break;
            case 'v' :
                _flagV(value);
                break;
            case 'h' :
                _flagH(value);
                break;
            case 'i' :
                _flagI(value);
                break;
            case 't' :
                _flagT(value);
                break;
        }
    }

    function setRegister(name, value) {
        var index = name.substr(1);
        if (value >= 0 && value < 256) {
            memory[index] = value;
        }
    }

    function setMemory(index, value) {
        if (value >= 0 && value < 256) {
            memory[index] = value;
        }
    }

    function setProgramMemory(index, value) {
        if (value >= 0 && value < 256) {
            program[index] = value;
        }
    }

    function getProgramCounterIndex() {
        return 2*counter;
    }

    function getProgramMaxSize() {
        return 64*1024*2;
    }

    return {
        load: load,
        reset: reset,
        next: next,

        getProgramCounter: getProgramCounter,
        getCycles: getCycles,
        getFlags: getFlags,
        getRegisters: getRegisters,
        getMemory: getMemory,
        getProgramMemory: getProgramMemory,

        setProgramCounter: setProgramCounter,
        setCycles: setCycles,
        setFlag: setFlag,
        setRegister: setRegister,
        setMemory: setMemory,
        setProgramMemory: setProgramMemory,

        getMemoryChoices: getMemoryChoices,
        getProgramCounterIndex: getProgramCounterIndex,
        getProgramMaxSize: getProgramMaxSize,
    };
});
