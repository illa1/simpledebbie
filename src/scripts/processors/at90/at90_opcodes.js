define(function() {

    var opcodes = {
        adc:  { pattern: '0001 11BA AAAA BBBB'},
        add:  { pattern: '0000 11BA AAAA BBBB'}, 
        adiw: { pattern: '1001 0110 BBAA BBBB', values1: [24, 26, 28, 30]},
        and:  { pattern: '0010 00BA AAAA BBBB'}, 
        andi: { pattern: '0111 BBBB AAAA BBBB', delta1: 16}, 
        asr:  { pattern: '1001 010A AAAA 0101'},
        bclr: { pattern: '1001 0100 1AAA 1000'},
        bld:  { pattern: '1111 100A AAAA 0BBB'},
        brbc: { pattern: '1111 01BB BBBB BAAA', positive2: 7},
        brbs: { pattern: '1111 00BB BBBB BAAA', positive2: 7},
        brcc: { pattern: '1111 01AA AAAA A000', positive1: 7},
        brcs: { pattern: '1111 00AA AAAA A000', positive1: 7},
        breq: { pattern: '1111 00AA AAAA A001', positive1: 7},
        brge: { pattern: '1111 01AA AAAA A100', positive1: 7},
        brhc: { pattern: '1111 01AA AAAA A101', positive1: 7},
        brhs: { pattern: '1111 00AA AAAA A101', positive1: 7},
        brid: { pattern: '1111 01AA AAAA A111', positive1: 7},
        brie: { pattern: '1111 00AA AAAA A111', positive1: 7},
        brlo: { pattern: '1111 00AA AAAA A000', positive1: 7},
        brlt: { pattern: '1111 00AA AAAA A100', positive1: 7},
        brmi: { pattern: '1111 00AA AAAA A010', positive1: 7},
        brne: { pattern: '1111 01AA AAAA A001', positive1: 7},
        brpl: { pattern: '1111 01AA AAAA A010', positive1: 7},
        brsh: { pattern: '1111 01AA AAAA A000', positive1: 7},
        brtc: { pattern: '1111 01AA AAAA A110', positive1: 7},
        brts: { pattern: '1111 00AA AAAA A110', positive1: 7},
        brtc: { pattern: '1111 01AA AAAA A011', positive1: 7},
        brts: { pattern: '1111 00AA AAAA A011', positive1: 7},
        bset: { pattern: '1001 0100 0AAA 1000'},
        bst:  { pattern: '1111 101A AAAA 0BBB'},
        call: { pattern: '1001 010A AAAA 111A AAAA AAAA AAAA AAAA'},
        cbi:  { pattern: '1001 1000 AAAA ABBB'},
        cbr:  { pattern: '0111 BBBB AAAA BBBB', delta1: 16, complement2: true}, //ANDI R, COM(K)
        clc:  { pattern: '1001 0100 1000 1000'},
        clh:  { pattern: '1001 0100 1101 1000'},
        cli:  { pattern: '1001 0100 1111 1000'},
        cln:  { pattern: '1001 0100 1010 1000'},
        clr:  { pattern: '0010 01BA AAAA BBBB'}, // EOR A, A
        cls:  { pattern: '1001 0100 1100 1000'},
        clt:  { pattern: '1001 0100 1110 1000'},
        clv:  { pattern: '1001 0100 1011 1000'},
        clz:  { pattern: '1001 0100 1001 1000'},
        com:  { pattern: '1001 010A AAAA 0000'},
        cp:   { pattern: '0001 01BA AAAA BBBB'},
        cpc:  { pattern: '0000 01BA AAAA BBBB'},
        cpi:  { pattern: '0011 BBBB AAAA BBBB', delta1: 16},
        cpse: { pattern: '0001 00BA AAAA BBBB'},
        dec:  { pattern: '1001 010A AAAA 1010'},
        eor:  { pattern: '0010 01BA AAAA BBBB'},
        in:   { pattern: '1011 0BBA AAAA BBBB'},
        inc:  { pattern: '1001 010A AAAA 0011'},
        jmp:  { pattern: '1001 010A AAAA 110A AAAA AAAA AAAA AAAA'},
        lac_z:  { pattern: '1001 001A AAAA 0110'},
        las_z:  { pattern: '1001 001A AAAA 0101'},
        lat_z:  { pattern: '1001 001A AAAA 0111'},
        ld_x:  { pattern: '1001 000A AAAA 1100'},
        ld_xp: { pattern: '1001 000A AAAA 1101'},
        ld_xm: { pattern: '1001 000A AAAA 1110'},
        ld_y:  { pattern: '1000 000A AAAA 1000'},
        ld_yp: { pattern: '1001 000A AAAA 1001'},
        ld_ym: { pattern: '1001 000A AAAA 1010'},
        ld_yq: { pattern: '10B0 BB0A AAAA 1BBB'},
        ld_z:  { pattern: '1000 000A AAAA 0000'},
        ld_zp: { pattern: '1001 000A AAAA 0001'},
        ld_zm: { pattern: '1001 000A AAAA 0010'},
        ld_zq: { pattern: '10B0 BB0A AAAA 0BBB'},
        ldi:  { pattern: '1110 BBBB AAAA BBBB', delta1: 16},
        lds:  { pattern: '1001 000A AAAA 0000 BBBB BBBB BBBB BBBB'},
        lpm:  { pattern:  '1001 0101 1100 1000'},
        lpm_z: { pattern: '1001 000A AAAA 0100'},
        lpm_zp:{ pattern: '1001 000A AAAA 0101'},
        lsl:  { pattern: '0000 11BA AAAA BBBB'}, //ADD RD RD
        lsr:  { pattern: '1001 010A AAAA 0110'},
        mov:  { pattern: '0010 11BA AAAA BBBB'},
        movw: { pattern: '0000 0001 AAAA BBBB', values1: [0,2,4,6,8,10,12,14,16,18,20,22,24,26,28,30], values2: [0,2,4,6,8,10,12,14,16,18,20,22,24,26,28,30]},
        mul:  { pattern: '1001 11BA AAAA BBBB'},
        muls: { pattern: '0000 0010 AAAA BBBB', delta1: 16, delta2: 16,},
        mulsu: { pattern: '0000 0011 0AAA 0BBB', values1: [16,17,18,19,20,21,22,23], values2: [16,17,18,19,20,21,22,23]},
        neg:  { pattern: '1001 010A AAAA 0001'},
        nop:  { pattern: '0000 0000 0000 0000'},
        or:   { pattern: '0010 10BA AAAA BBBB'},
        ori:  { pattern: '0110 BBBB AAAA BBBB', delta1: 16},
        out:  { pattern: '1011 1AAB BBBB AAAA'},
        pop:  { pattern: '1001 000A AAAA 1111'},
        push: { pattern: '1001 001A AAAA 1111'},
        rcall: { pattern: '1101 AAAA AAAA AAAA', positive1: 12},
        ret:  { pattern: '1001 0101 0000 1000'},
        reti: { pattern: '1001 0101 0001 1000'},
        rjmp: { pattern: '1100 AAAA AAAA AAAA', positive1: 12},
        rol:  { pattern: '0001 11BA AAAA BBBB'},//ADC RD, RD
        ror:  { pattern: '1001 010A AAAA 0111'},
        sbc:  { pattern: '0000 10BA AAAA BBBB'},
        sbci: { pattern: '0100 BBBB AAAA BBBB', delta1: 16},
        sbi:  { pattern: '1001 1010 AAAA ABBB'},
        sbic: { pattern: '1001 1001 AAAA ABBB'},
        sbis: { pattern: '1001 1011 AAAA ABBB'},
        sbiw: { pattern: '1001 0111 BBAA BBBB', values1: [24,26,28,30]},
        sbr:  { pattern: '0110 BBBB AAAA BBBB', delta1: 16}, 
        sbrc: { pattern: '1111 110A AAAA 0BBB'},
        sbrs: { pattern: '1111 111A AAAA 0BBB'},
        sec:  { pattern: '1001 0100 0000 1000'},
        seh:  { pattern: '1001 0100 0101 1000'},
        sei:  { pattern: '1001 0100 0111 1000'},
        sen:  { pattern: '1001 0100 0010 1000'},
        ser:  { pattern: '1110 1111 AAAA 1111'},
        ses:  { pattern: '1001 0100 0100 1000'},
        set:  { pattern: '1001 0100 0110 1000'},
        sev:  { pattern: '1001 0100 0011 1000'},
        sez:  { pattern: '1001 0100 0001 1000'},
        st_x:  { pattern: '1001 001A AAAA 1100'},
        st_xp: { pattern: '1001 001A AAAA 1101'},
        st_xm: { pattern: '1001 001A AAAA 1110'},
        st_y:  { pattern: '1000 001A AAAA 1000'},
        st_yp: { pattern: '1001 001A AAAA 1001'},
        st_ym: { pattern: '1001 001A AAAA 1010'},
        st_yq: { pattern: '10B0 BB1A AAAA 1BBB'},
        st_z:  { pattern: '1000 001A AAAA 0000'},
        st_zp: { pattern: '1001 001A AAAA 0001'},
        st_zm: { pattern: '1001 001A AAAA 0010'},
        st_zq: { pattern: '10B0 BB1A AAAA 0BBB'},
        sts:  { pattern: '1001 001B BBBB 0000 AAAA AAAA AAAA AAAA'},
        sub:  { pattern: '0001 10BA AAAA BBBB'},
        subi: { pattern: '0101 BBBB AAAA BBBB', delta1: 16},
        swap: { pattern: '1001 010A AAAA 0010'},
        tst:  { pattern: '0010 00BA AAAA BBBB'}, //and a, a,
        xch_z:  { pattern: '1001 001A AAAA 0100'},
    };

    function _toBitNumber(command, number, arg) {
        if (arg == 1) {
            if (opcodes[command].delta1 != undefined) {
                number -= opcodes[command].delta1;
            }
            else if (opcodes[command].values1 != undefined) {
                number = opcodes[command].values1.indexOf(number);
            }
            else if (opcodes[command].positive1 != undefined) {
                if (number < 0) {
                    number = _complement(number, opcodes[command].positive1);
                }
            }
        } else {
            if (opcodes[command].delta2 != undefined) {
                number -= opcodes[command].delta2;
            }
            else if (opcodes[command].values2 != undefined) {
                number = opcodes[command].values2.indexOf(number);
            }
            else if (opcodes[command].positive2 != undefined) {
                if (number < 0) {
                    number = _complement(number, opcodes[command].positive2);
                }
            }
        }
        return number;
    }

    function _fromBitNumber(command, number, arg) {
        if (arg == 1) {
            if (opcodes[command].delta1 != undefined) {
                number += opcodes[command].delta1;
            }
            else if (opcodes[command].values1 != undefined) {
                number = opcodes[command].values1[number];
            }
            else if (opcodes[command].positive1 != undefined) {
                if (number > Math.pow(2, opcodes[command].positive1 - 1)) {
                    number = - _complement(number, opcodes[command].positive1);
                }
            }
        } else {
            if (opcodes[command].delta2 != undefined) {
                number += opcodes[command].delta2;
            }
            else if (opcodes[command].values2 != undefined) {
                number = opcodes[command].values2[number];
            }
            else if (opcodes[command].positive2 != undefined) {
                if (number >= Math.pow(2, opcodes[command].positive2 - 1)) {
                    number = - _complement(number, opcodes[command].positive2);
                }
            }
        }
        return number;
    }

    function _complement(number, size){
        if (number < 0) {
            number = - number;
        }
        number = Math.pow(2, size) - number;
        return number;
    }

    function _count(character, string) {
        var result = 0,
            i = 0;
        for (i = 0; i < string.length; i += 1) {
            if (string[i] == character) {
                result += 1;
            } 
        }
        return result;
    }

    function _adjust(bits, length) {
        while (bits.length < length) {
            bits = '0' + bits;
        }
        return bits.substr(0, length);
    }

    function _revertBytes(opcode) {
        var result = [],
            i;
        for (i = 0; i < opcode.length; i += 16) {
            result = result.concat(opcode.slice(i+8, i+16));
            result = result.concat(opcode.slice(i, i+8));
        }
        return result;
    }

    function toOpcode(name, arg1, arg2) {
        var opcode, 
            arg1bits,
            arg2bits,
            i = 0,
            i1 = 0,
            i2 = 0;

        if (arg1 != undefined && arg1.value != undefined && arg1.delta != undefined) {
            name += '_' + arg1.value;
            if (arg1.delta == 1) {
                name += 'p';
            } else if (arg1.delta == -1) {
                name += 'm';
            } else if (arg1.delta != 0) {
                name += 'q';
            }
            arg1 = arg2;
            arg2 = undefined;
        }
        if (arg2 != undefined &&  arg2.value != undefined && arg2.delta != undefined) {
            name += '_' + arg2.value;
            if (arg2.delta == 1) {
                name += 'p';
            } else if (arg2.delta == -1) {
                name += 'm';
            } else if (arg2.delta != 0) {
                name += 'q';
            }
            arg2 = undefined;
        }
        
        opcode = opcodes[name].pattern.replace(/ /g, '').split('');
        if (arg1 != undefined) { 
            arg1bits = _adjust(_toBitNumber(name, arg1, 1).toString(2), _count('A', opcode));
        }
        if (arg2 != undefined) { 
            if (opcodes[name].complement2 != undefined) {
                arg2 = _complement(arg2, _count('B', opcode));
            }
            arg2bits = _adjust(_toBitNumber(name, arg2, 2).toString(2), _count('B', opcode));
        } else {
            arg2bits = arg1bits;
        }
        
        for (i = 0; i < opcode.length; i += 1) {
            if (opcode[i] == 'A') {
                opcode[i] = arg1bits[i1];
                i1++;
            }
            if (opcode[i] == 'B') {
                opcode[i] = arg2bits[i2];
                i2++;
            }
        }
        opcode = _revertBytes(opcode);
        opcode = opcode.join('');
        bytes = [];
        for (i = 0; i < opcode.length; i += 8) {
            bytes.push(parseInt(opcode.substr(i, 8), 2));
        }
        return bytes;
    }
    
    function fromOpcode(bytes) {
        var opcode,
            regex,
            name,
            pattern,
            arg1 = 0,
            arg2 = 0;
        opcode = '';
        for (var i = 0; i < bytes.length; i += 1) {
            opcode += _adjust(bytes[i].toString(2), 8);
        }
        
        opcode = _revertBytes(opcode).join('');
        for (o in opcodes) {
            regex = new RegExp('^' + opcodes[o].pattern.replace(/ /g, '').replace(/[AB]/g, '.') + '$');
            if (regex.test(opcode) == true) {
                name = o;
                pattern = opcodes[o].pattern.replace(/ /g, '');
                break;
            }
        }
        if (pattern == undefined) {
            return null;
        }
        for (i = 0; i < pattern.length; i += 1) {
            if (pattern[i] == 'A') {
                if (opcode[i] == '1') {
                    arg1 += 1;
                }
                arg1 *= 2;
            }
            if (pattern[i] == 'B') {
                if (opcode[i] == '1') {
                    arg2 += 1;
                }
                arg2 *= 2;
            }
        }
        arg1 /= 2;
        arg2 /= 2;
        arg1 = _fromBitNumber(name, arg1, 1);
        arg2 = _fromBitNumber(name, arg2, 2);
        
        return {
            name: name,
            args: [arg1, arg2],
        };
    }

    return {
        toOpcode: toOpcode,
        fromOpcode: fromOpcode,
    };

});
