ace.define('ace/mode/assembly_atmel_highlight_rules', ['require', 'exports', 'module'], function(require, exports, module) {
"use strict";

var oop = require("ace/lib/oop");
var TextHighlightRules = require("./text_highlight_rules").TextHighlightRules;

var AssemblyAtmelHighlightRules = function() {

    this.$rules = { 'start': 
       [ { token: 'keyword.control.assembly',
           regex: '\\b(?:adc|add|adiw|and|andi|asr|bclr|bld|brbc|brbs|brcc|brcs|break|breq|brge|brhc|brhs|brid|brie|brlo|brlt|brmi|brne|brpl|brsh|brtc|brts|brvc|brvs|bset|bst|call|cbi|cbr|clc|clh|cli|cln|clr|cls|clt|clv|clz|com|cp|cpc|cpi|cpse|dec|des|eor|in|inc|jmp|lac|las|lat|ld|ldi|lds|lpm|lsl|lsr|mov|movw|mul|muls|mulsu|neg|nop|or|ori|out|pop|push|rcall|ret|reti|rjmp|rol|ror|sbc|sbci|sbi|sbic|sbis|sbiw|sbr|sbrc|sbrs|sec|seh|sei|sen|ser|ses|set|sev|sez|sleep|st|sts|sub|subi|swap|tst)\\b',
           caseInsensitive: true },
         { token: 'variable.parameter.register.assembly',
           regex: '\\b(?:r\\d|r[1-2]\\d|r3[0-1])\\b',
           caseInsensitive: true },
         { token: 'constant.character.decimal.assembly',
           regex: '\\b[0-9]+\\b' },
         { token: 'constant.character.hexadecimal.assembly',
           regex: '\\b0x[A-F0-9]+\\b',
           caseInsensitive: true },
         { token: 'constant.character.hexadecimal.assembly',
           regex: '\\b\\$[A-F0-9]+\\b',
           caseInsensitive: true },
         { token: 'constant.character.binary.assembly',
           regex: '\\b0b[01]+\\b',
           caseInsensitive: true },
         { token: 'support.function.directive.assembly',
           regex: '(^|\\s*)+\\.(byte|cseg|db|dd|def|dseg|dq|dw|equ|org|set|undef)+\\b',
           caseInsensitive: true },
         { token: 'entity.name.function.assembly', regex: '^(\\s*\\w+:)+' },
         { token: 'comment.assembly', regex: ';.*$' } ] 
    };
    
    this.normalizeRules();
};

oop.inherits(AssemblyAtmelHighlightRules, TextHighlightRules);

exports.AssemblyAtmelHighlightRules = AssemblyAtmelHighlightRules;

});

ace.define("ace/mode/folding/assembly_atmel",["require","exports","module"], function(require, exports, module) {
"use strict";

var oop = require("ace/lib/oop");
var BaseFoldMode = require("./fold_mode").FoldMode;
var Range = require("ace/range").Range;

var FoldMode = exports.FoldMode = function() {};
oop.inherits(FoldMode, BaseFoldMode);

(function() {

    this.getFoldWidgetRange = function(session, foldStyle, row) {
        return this.indentationBlock(session, row);
    };
    this.getFoldWidget = function(session, foldStyle, row) {
        var line = session.getLine(row);
        var indent = line.search(/\S/);
        var next = session.getLine(row + 1);
        var prev = session.getLine(row - 1);
        var prevIndent = prev.search(/\S/);
        var nextIndent = next.search(/\S/);

        if (indent == -1) {
            session.foldWidgets[row - 1] = prevIndent!= -1 && prevIndent < nextIndent ? "start" : "";
            return "";
        }
        if (prevIndent == -1) {
            if (indent == nextIndent && line[indent] == "#" && next[indent] == "#") {
                session.foldWidgets[row - 1] = "";
                session.foldWidgets[row + 1] = "";
                return "start";
            }
        } else if (prevIndent == indent && line[indent] == "#" && prev[indent] == "#") {
            if (session.getLine(row - 2).search(/\S/) == -1) {
                session.foldWidgets[row - 1] = "start";
                session.foldWidgets[row + 1] = "";
                return "";
            }
        }

        if (prevIndent!= -1 && prevIndent < indent)
            session.foldWidgets[row - 1] = "start";
        else
            session.foldWidgets[row - 1] = "";

        if (indent < nextIndent)
            return "start";
        else
            return "";
    };

}).call(FoldMode.prototype);

});

ace.define("ace/mode/assembly_atmel",["require","exports","module"], function(require, exports, module) {
"use strict";

var oop = require("ace/lib/oop");
var TextMode = require("./text").Mode;
var AssemblyAtmelHighlightRules = require("./assembly_atmel_highlight_rules").AssemblyAtmelHighlightRules;
var FoldMode = require("ace/mode/folding/assembly_atmel").FoldMode;

var Mode = function() {
    this.HighlightRules = AssemblyAtmelHighlightRules;
    this.foldingRules = new FoldMode();
};
oop.inherits(Mode, TextMode);

(function() {
    this.lineCommentStart = ";";
    this.$id = "ace/mode/assembly_atmel";
}).call(Mode.prototype);

exports.Mode = Mode;
});
