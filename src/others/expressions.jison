/* expressions computing */

/* regex */
%lex
%%

"low"			return 'LOW'
"high"			return 'HIGH'
"byte2"			return 'HIGH'
"byte3"			return 'BYTE3'
"byte4"			return 'BYTE4'
"lwrd"			return 'LWRD'
"hwrd"			return 'HWRD'
"page"			return 'PAGE'
"exp2"			return 'EXP2'
"log2"			return 'LOG2'

0b[01]+			return 'BIN_NUMBER'
0x[0-9a-f]+		return 'HEX_NUMBER1'
0[0-9]+			return 'OCT_NUMBER'
[0-9][0-9]*		return 'DEC_NUMBER'
$[0-9a-f]+		return 'HEX_NUMBER2'

"("             return 'LEFT'
")"             return 'RIGHT'

"*"				return 'MUL'
"/"             return 'DIV'
"%"             return 'MOD'
"-"             return 'MINUS'
"+"             return 'PLUS'

">>"			return 'BIT_RIGHT'
"<<"			return 'BIT_LEFT'

"=="            return 'EQUAL'
"!="			return 'NOT_EQUAL'
"<="            return 'LESS_EQUAL'
">="            return 'GREATER_EQUAL'
"<"             return 'LESS'
">"             return 'GREATER'

"^"				return 'BIT_XOR'
"~"				return 'BIT_NOT'
"!"				return 'LOG_NOT'
"&&"			return 'LOG_AND'
"&"				return 'BIT_AND'
"||"			return 'LOG_OR'
"|"				return 'BIT_OR'

<<EOF>>               return 'EOF'
\s+                   /* skip whitespace */
.                     return 'INVALID'

/lex

/* operators */

%left LOG_OR //4
%left LOG_AND //5
%left BIT_OR //6
%left BIT_XOR //7
%left BIT_AND //8
%left EQUAL NOT_EQUAL //9, none
%left LESS LESS_EQUAL GREATER GREATER_EQUAL //10, none
%left BIT_LEFT BIT_RIGHT //11
%left PLUS MINUS BIT_NOT LOG_NOT //12, none
%left MUL DIV MOD //13
%left UMINUS //14 none

%start expressions

%% /* grammar */

expressions
    : exp EOF						{return $1;}
    ;

exp
    : exp PLUS exp					{$$ = $1+$3;}
    | exp MINUS exp					{$$ = $1-$3;}
    | exp MUL exp					{$$ = $1*$3;}
    | exp DIV exp					{$$ = $1/$3;}
    | exp MOD exp					{$$ = $1%$3;}
    | MINUS exp %prec UMINUS		{$$ = -$2;}
    | LEFT exp RIGHT	        	{$$ = $2;}

    | DEC_NUMBER					{$$ = parseInt(yytext, 10);}
    | BIN_NUMBER					{$$ = parseInt(yytext.substr(2), 2);}
    | OCT_NUMBER					{$$ = parseInt(yytext, 8);}
    | HEX_NUMBER1					{$$ = parseInt(yytext, 16);}
    | HEX_NUMBER2					{$$ = parseInt(yytext.substr(1), 16);}
    
	| exp EQUAL exp			{if ($1 == $3) $$ = 1; else $$ = 0;}
	| exp NOT_EQUAL exp		{if ($1 != $3) $$ = 1; else $$ = 0;}
	| exp LESS exp			{if ($1 <  $3) $$ = 1; else $$ = 0;}
	| exp LESS_EQUAL exp	{if ($1 <= $3) $$ = 1; else $$ = 0;}
	| exp GREATER exp		{if ($1 >  $3) $$ = 1; else $$ = 0;}
	| exp GREATER_EQUAL exp	{if ($1 >= $3) $$ = 1; else $$ = 0;}
	
	| exp LOG_AND exp		{if ($1 != 0 && $3 != 0) $$ = 1; else $$ = 0;}	
	| exp LOG_OR exp		{if ($1 != 0 || $3 != 0) $$ = 1; else $$ = 0;}	
	| LOG_NOT exp			{if ($2 == 0) $$ = 1; else $$ = 0;}

	| exp BIT_AND exp		{$$ = $1 & $3;} 
	| exp BIT_OR exp		{$$ = $1 | $3;}	
	| exp BIT_XOR exp		{$$ = $1 ^ $3;}	
	| exp BIT_LEFT exp		{$$ = $1 << $3;}	
	| exp BIT_RIGHT exp		{$$ = $1 >> $3;}	
	| BIT_NOT exp			{$$ = ~ $2;}
		
	| LOW LEFT exp RIGHT		{$$ = $3 % 256;}
	| HIGH LEFT exp RIGHT		{$$ = ($3 >> 8) % 256;}
	| BYTE3 LEFT exp RIGHT		{$$ = ($3 >> 16) % 256;}
	| BYTE4 LEFT exp RIGHT		{$$ = ($3 >> 24) % 256;}
	| LWRD LEFT exp RIGHT		{$$ = $3 % (1<<16);}
	| HWRD LEFT exp RIGHT		{$$ = ($3 >> 16) % (1 << 16);}
	| PAGE LEFT exp RIGHT		{$$ = ($3 >> 16) % (1 << 6);}
	| EXP2 LEFT exp RIGHT		{$$ = 1 << $3;}
	| LOG2 LEFT exp RIGHT		{$$ = Math.floor(Math.log($3)/Math.log(2));}
    ;
