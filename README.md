# Simple Debbie

Działająca aplikacja znajduje się pod adresem: https://illa1.gitlab.io/simpledebbie. Powstała ona w ramach projektu inżynierskiego.

***

W aplikacji zostały wykorzystane:
+    JQuery      http://jquery.com/
+    JQueryUI    http://jqueryui.com/
+    Bootstrap   http://getbootstrap.com/
+    RequireJS   http://requirejs.org/
+    Underscore  http://underscorejs.org/
+    Ace         http://ace.c9.io/
+    Jison       http://zaach.github.io/jison/

***

Tworzenie zminifikowanej wersji aplikacji wymaga wykonania dwóch komend w głównym katalogu:
```
    node tools/r.js -o tools/build.js
    node tools/copy_files.js
```

***

![debbie](/uploads/45f2cb2025e17eb682e50e8cdd5bcb13/debbie.png)
