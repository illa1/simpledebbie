var fs = require('fs');

var copyRecursive = function(path1, path2) {
    var stats = fs.statSync(path1);
    var files; 
    if (stats.isDirectory()) {
        if (fs.existsSync(path2) == false) {
            fs.mkdirSync(path2);
        }
        files = fs.readdirSync(path1);
        files.forEach( function (f) {
            copyRecursive(path1 + '/' + f, path2 + '/' + f);
        });
    } else {
        copyFile(path1, path2);
    }
};

var copyFile = function(path1, path2) {
    fs.createReadStream(path1).pipe(fs.createWriteStream(path2));
};

copyRecursive('src/css','min/css');
copyRecursive('src/fonts','min/fonts');
//copyFile('src/index.html','min/index.html');
copyFile('src/about.html','min/about.html');

fs.readFile('src/index.html', 'utf-8', function(err, data) {
    var newData = data.replace(/<script data-main='scripts\/main' src='lib\/require.js'><\/script>/,
                              "<script src='main_min.js'></script>");
    fs.writeFileSync('min/index.html', newData);
});
